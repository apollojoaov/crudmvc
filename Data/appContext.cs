﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CRUDMVC.Models;

namespace CRUDMVC.Data
{
    public partial class appContext : DbContext
    {
            public virtual DbSet<Clientes> Clientes { get; set; }
        public appContext()
        {
            
        }

        public appContext(DbContextOptions<appContext> options)
            : base(options)
        {
            
        }   

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlite("Data Source=app.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
